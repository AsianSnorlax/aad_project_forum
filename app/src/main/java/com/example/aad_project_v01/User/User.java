package com.example.aad_project_v01.User;

public class User {
    private String id;
    private String name;
    private String imageURL;
    private String onlineStatus;
    private String search;
    private String status;

    public User(String id, String name, String imageURL, String onlineStatus, String search, String status){
        this.id = id;
        this.name = name;
        this.imageURL = imageURL;
        this.onlineStatus = onlineStatus;
        this.search = search;
        this.status = status;
    }

    public String getOnlineStatus() {
        return onlineStatus;
    }

    public void setOnlineStatus(String onlineStatus) {
        this.onlineStatus = onlineStatus;
    }

    public User(){ }

    public String getstatus() {
        return status;
    }

    public void setstatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
