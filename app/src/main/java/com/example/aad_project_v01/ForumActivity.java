package com.example.aad_project_v01;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.aad_project_v01.Adaptor.CommentAdaptor;
import com.example.aad_project_v01.Extra.GlobalVar;
import com.example.aad_project_v01.Forum.Forum;
import com.example.aad_project_v01.Forum.ForumPost;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This Activity is to show the forum Meta Data and to allow users to post comments. Forum Thread Activity
 * @author Alex Tay
 * @since 14/10/19
 * @version Developer
 */

public class ForumActivity extends AppCompatActivity {

    //Forum Detials
    TextView forum_Title;
    TextView forum_Contents;
    ImageView forum_Image;
    TextView poster_Name;
    TextView comments_Count;

    ImageButton btn_Send;
    EditText message_Send;
    CommentAdaptor commentAdapter;
    List<ForumPost> mForumPost;
    RecyclerView recyclerView;
    FirebaseUser fbUser;
    DatabaseReference dbReference;
    Intent intent;
    ValueEventListener seenListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum);

        recyclerView = findViewById(R.id.recycler_view1);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);

        forum_Image = findViewById(R.id.forum_thread_image);
        poster_Name = findViewById(R.id.forum_thread_poster);
        forum_Title = findViewById(R.id.forum_thread_title);
        forum_Contents = findViewById(R.id.forum_thread_contents);
        comments_Count = findViewById(R.id.forum_thread_comments_count);
        
        btn_Send = findViewById(R.id.forum_btn_send);
        message_Send = findViewById(R.id.forum_text_send);

        intent = getIntent();
        final String threadId = intent.getStringExtra("threadId");
        fbUser = FirebaseAuth.getInstance().getCurrentUser();

        //Set what happens when you press the send button(Black Arrow)
        btn_Send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = message_Send.getText().toString();        //Collect sent message as String
                if (!message.equals("")){
                    postMessage(threadId, message);      //Give String to method that uploads the message to the database
                }else{
                    Toast emptyMessage = Toast.makeText(ForumActivity.this, "There is nothing to send", Toast.LENGTH_LONG);
                    emptyMessage.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 1250);
                    emptyMessage.show();
                }
                message_Send.setText("");
            }
        });

        //Update Profile Picture and Name of Posters Automatically
        dbReference = FirebaseDatabase.getInstance().getReference("Forum").child(threadId);
        //Load Image and Name of every newly registered account
        dbReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Forum forum = dataSnapshot.getValue(Forum.class);
                    poster_Name.setText("User: " + forum.getPosterName());
                    forum_Title.setText(forum.getTitle());
                    forum_Contents.setText(forum.getContents());
                    comments_Count.setText(Long.toString(forum.getCommentsCount()));

                    if (forum.getFilePath().equals("default")) {
                        forum_Image.setImageResource((R.mipmap.ic_launcher));
                    } else {
                        Glide.with(getApplicationContext()).load(forum.getFilePath()).into(forum_Image);
                    }
                    readMessages(threadId);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void readMessages(final String threadId){
        mForumPost = new ArrayList<>();

        dbReference = FirebaseDatabase.getInstance().getReference("ForumPost").child(threadId);

        dbReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    mForumPost.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        ForumPost forumPost = snapshot.getValue(ForumPost.class);
                        mForumPost.add(forumPost);

                        commentAdapter = new CommentAdaptor(ForumActivity.this, mForumPost);
                        recyclerView.setAdapter(commentAdapter);
                    }
                    DatabaseReference updateCommentCount = FirebaseDatabase.getInstance().getReference("Forum").child(threadId);
                    HashMap<String, Object> commentsCount = new HashMap<>();
                    commentsCount.put("commentsCount", dataSnapshot.getChildrenCount());
                    Log.i("childrencount", Long.toString(dataSnapshot.getChildrenCount()));
                    updateCommentCount.updateChildren(commentsCount);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }
    //Send Forum Post to Database along with the relevant information (Forum Message, PosterId)
    private void postMessage (final String threadId, String message) {
        DatabaseReference forumThread = FirebaseDatabase.getInstance().getReference("ForumPost").child(threadId);

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("forumMessage", message);
        hashMap.put("name", GlobalVar.getPosterName());
        hashMap.put("imageURL", GlobalVar.getImageUrl());
        hashMap.put("status", GlobalVar.getStatus());
        forumThread.push().setValue(hashMap);
    }
}