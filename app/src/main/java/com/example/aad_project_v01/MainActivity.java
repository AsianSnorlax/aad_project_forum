package com.example.aad_project_v01;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.aad_project_v01.Extra.GlobalVar;
import com.example.aad_project_v01.Fragment.ChatFragment;
import com.example.aad_project_v01.Fragment.ForumFragment;
import com.example.aad_project_v01.Fragment.ProfileFragment;
import com.example.aad_project_v01.Fragment.UserFragment;
import com.example.aad_project_v01.User.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * This Activity to house the four fragments (Chat, User, Forum, Profile)
 * @author Alex Tay
 * @since 14/10/19
 * @version Developer
 */

public class MainActivity extends AppCompatActivity {

    CircleImageView profile_Image;
    TextView name;

    FirebaseAuth auth;
    FirebaseUser firebaseUser;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        GlobalVar.setPassword("qwerty");
        profile_Image = findViewById(R.id.profile_image);
        name = findViewById(R.id.name);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Users").child(firebaseUser.getUid());
        //Set Status to Online
        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap.put("onlineStatus", "Online");
        reference.updateChildren(hashMap);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);                      //Store database information as an object
                GlobalVar.setStatus(user.getstatus());
                GlobalVar.setPosterName(user.getName());
                GlobalVar.setImageUrl(user.getImageURL());
                name.setText(user.getName());
                if (user.getImageURL().equals("default")){
                    profile_Image.setImageResource(R.mipmap.ic_launcher);
                }else{
                    Glide.with(getApplicationContext()).load(user.getImageURL()).into(profile_Image);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        TabLayout tabLayout = findViewById(R.id.tab_Layout);
        ViewPager viewPager = findViewById(R.id.view_Pager);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        //Create New Fragments
        viewPagerAdapter.addFragment(new UserFragment(),"Users");
        viewPagerAdapter.addFragment(new ChatFragment(),"My");
        viewPagerAdapter.addFragment(new ForumFragment(),"Forum");
        viewPagerAdapter.addFragment(new ProfileFragment(),"Profile");

        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

    }
    //Initialise Menu(...) at the top right corner
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the dotdotdot; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dotdotdot, menu);
        return true;
    }

    @Override
    //Configure what happens when you press the items under ... at the top right
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.logout:{
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Confirm Logout").setCancelable(true)
                        // (true) dialog can be ignored by pressing the back button
                        // Confirm Logout
                        .setPositiveButton("Logout",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        HashMap<String,Object> hashMap = new HashMap<>();
                                        hashMap.put("onlineStatus", "Offline");
                                        reference.updateChildren(hashMap);

                                        FirebaseAuth.getInstance().signOut();
                                        startActivity(new Intent(MainActivity.this, StartActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                        finish();
                                    }
                                })
                        // Misclick
                        .setNegativeButton("Back",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                }).show();
            }//End of Logout
            return true;
            case R.id.verify:{
                if (!GlobalVar.getStatus().equals("Admin")) {
                    LayoutInflater li = LayoutInflater.from(MainActivity.this);
                    View promptsView = li.inflate(R.layout.prompts, null);

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            MainActivity.this);

                    // set prompts.xml to alertdialog builder
                    alertDialogBuilder.setView(promptsView);

                    final EditText userInput = (EditText) promptsView
                            .findViewById(R.id.admin_password_input);

                    // set dialog message
                    alertDialogBuilder
                            .setCancelable(true)
                            .setPositiveButton("Submit",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            String password = GlobalVar.getPassword();
                                            String enteredPassword = userInput.getText().toString();
                                            if (enteredPassword.equals(password)) {
                                                HashMap<String, Object> hashMap = new HashMap<>();
                                                hashMap.put("status", "Admin");
                                                reference.updateChildren(hashMap);
                                                //Display Congratulations Message
                                                Toast admin = Toast.makeText(MainActivity.this, "You are now an Admin!", Toast.LENGTH_LONG);
                                                admin.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 1250);
                                                admin.show();
                                                GlobalVar.setStatus("Admin");
                                            } else {
                                                Toast admin = Toast.makeText(MainActivity.this, "Try Again", Toast.LENGTH_LONG);
                                                admin.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 1250);
                                                admin.show();
                                            }

                                        }
                                    })
                            .setNegativeButton("Cancel",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    }).show();


                }
                else{
                    Toast admin = Toast.makeText(MainActivity.this, "You are already an Admin!", Toast.LENGTH_LONG);
                    admin.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 1250);
                    admin.show();
                }
            }
            return true;
        }
        return false;
    }
    class ViewPagerAdapter extends FragmentPagerAdapter{
        private ArrayList<Fragment>fragments;
        private ArrayList<String>titles;

        ViewPagerAdapter(FragmentManager fm){
            super(fm);
            this.fragments = new ArrayList<>();
            this.titles = new ArrayList<>();
        }
        @Override
        public Fragment getItem(int i) {
            return fragments.get(i);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
        public void addFragment(Fragment fragment, String title){
            fragments.add(fragment);
            titles.add(title);

        }
        @Nullable
        @Override
        public CharSequence getPageTitle(int i){
            return titles.get(i);
        }
    }//End of ViewPageAdaptor Class
    private void status (String status){
        reference = FirebaseDatabase.getInstance().getReference("Users").child(firebaseUser.getUid());

        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap.put("onlineStatus", status);

        reference.updateChildren(hashMap);
    }
    @Override
    protected void onResume(){
        super.onResume();
        status("Online");
    }
    @Override
    protected void onPause(){
        super.onPause();
        status("Offline");
    }
    @Override
    public void onBackPressed() {
        //do nothing
    }
}//End of MainActivity Class
