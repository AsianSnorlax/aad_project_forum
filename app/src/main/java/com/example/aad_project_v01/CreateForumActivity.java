package com.example.aad_project_v01;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.aad_project_v01.Extra.GlobalVar;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * This Activity is used to Create the Forum (Title, Contents, Photo)
 *
 * @author Alex Tay
 * @version Developer
 * @since 14/10/19
 */

public class CreateForumActivity extends AppCompatActivity {
    DatabaseReference dbReference;

    DatabaseReference frompath;
    FirebaseUser fbUser;
    StorageReference storageReference = FirebaseStorage.getInstance().getReference("uploads");
    DatabaseReference forumReference;

    MaterialEditText forumTitle;
    MaterialEditText forumContents;
    ImageView picturePreview;
    Boolean gotPicture = false;

    Button uploadPhoto;
    Button uploadForumPost;

    private static final int IMAGE_REQUEST = 1;
    private Uri imageUri;
    private StorageTask uploadTask;
    Uri captureImageUri;
    String currentPhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_forum);
        //Upload Picture in Forum Discussion Creation
        uploadPhoto = findViewById(R.id.forum_contents_photo_upload);
        uploadPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder uploadForumPhoto = new AlertDialog.Builder(CreateForumActivity.this);
                uploadForumPhoto.setMessage("Choose Image Location").setCancelable(true)
                        // (true) dialog can be ignored by pressing the back button
                        // Upload Profile Picture from Memory
                        .setPositiveButton("From Memory",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        openImage();
                                    }
                                })
                        .setNegativeButton("Capture Photo",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dispatchTakePictureIntent();
                                    }
                                }).show();
            }
        });
        uploadForumPost = findViewById(R.id.forum_contents_submit);
        uploadForumPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forumTitle = findViewById(R.id.forum_title);
                String forum_Title = forumTitle.getText().toString();
                forumContents = findViewById(R.id.forum_contents);
                String forum_Contents = forumContents.getText().toString();
                fbUser = FirebaseAuth.getInstance().getCurrentUser();
                String user_Id = fbUser.getUid();
                String databasePath;
                //If Forum Title is empty or contents are lesser than 8 Characters.
                if (TextUtils.isEmpty(forum_Title) || forum_Title.length() <= 4) {
                    Toast noForumTitle = Toast.makeText(CreateForumActivity.this, "Invalid Forum Title and/or Contents", Toast.LENGTH_LONG);
                    noForumTitle.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 1250);
                    noForumTitle.show();
                } else {

                    createForum(forum_Title, forum_Contents, user_Id);
                    //Wipe Memory
                    gotPicture = false;
                    //Go back to Main Activity
                    Intent goBack = new Intent(CreateForumActivity.this, MainActivity.class);
                    startActivity(goBack);
                    finish();
                }

            }
        });
    }

    private void createForum(String title, String contents, String id) {
        String forumId = title + '_' + id;
        frompath = FirebaseDatabase.getInstance().getReference("Transition").child("filePath");
        forumReference = FirebaseDatabase.getInstance().getReference("Forum").child(forumId);
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("title", title);
        hashMap.put("contents", contents);
        hashMap.put("filePath", "default");
        hashMap.put("posterId", id);
        hashMap.put("searchTitle", title.toLowerCase());
        hashMap.put("commentsCount", 0);
        hashMap.put("posterName", GlobalVar.getPosterName());
        forumReference.setValue(hashMap);
        if (gotPicture == true) {
            final DatabaseReference topath = FirebaseDatabase.getInstance().getReference("Forum").child(forumId).child("filePath");
            moveRecord(frompath, topath);
        }

    }

    private void moveRecord(DatabaseReference fromPath, final DatabaseReference toPath) {
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                toPath.setValue(dataSnapshot.getValue()).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isComplete()) {
                        } else {
                        }
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        fromPath.addListenerForSingleValueEvent(valueEventListener);
    }
    //Upload Forum Image to Database

    private void uploadImage() {
        if (imageUri != null) {
            final StorageReference fileReference = storageReference.child(System.currentTimeMillis()
                    + "." + getFileExtension(imageUri));
            uploadTask = fileReference.putFile(imageUri);
            uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return fileReference.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        dbReference = FirebaseDatabase.getInstance().getReference("Transition");
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("filePath", downloadUri.toString());
                        dbReference.updateChildren(map);
                    } else {
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                }
            });
        } else {
        }
    }

    //Take photo. After Taking photo, shows 3 options: Retake, Keep, Leave.
    static final int REQUEST_TAKE_PHOTO = 2;

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        // Create the File where the photo should go
        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException ex) {
            // Error occurred while creating the File
        }
        // Continue only if the File was successfully created
        if (photoFile != null) {
            captureImageUri = FileProvider.getUriForFile(getApplicationContext(),
                    "com.example.aad_project_v01",
                    photoFile);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, captureImageUri);
            startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
        }
    }

    //Create File for Photo
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void openImage() {
        Intent intentImage = new Intent();
        intentImage.setType("image/*");
        intentImage.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intentImage, IMAGE_REQUEST);
    }

    private String getFileExtension(Uri uri) {
        ContentResolver contentResolver = getApplicationContext().getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //From Memory
        if (requestCode == IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            imageUri = data.getData();
        }
        //From Camera
        if (requestCode == REQUEST_TAKE_PHOTO) {
            imageUri = captureImageUri;
        }
        if (uploadTask != null && uploadTask.isInProgress()) {
            Toast.makeText(getApplicationContext(), "Waiting.....", Toast.LENGTH_SHORT).show();
        } else {
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                picturePreview = findViewById(R.id.forum_contents_picture_preview);
                picturePreview.setImageBitmap(bitmap);
                gotPicture = true;
                uploadImage();
                SystemClock.sleep(500);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
