package com.example.aad_project_v01.Extra;

public class GlobalVar {

    public static String status;

    private static String posterName;

    private static String imageUrl;
    private static String forumPicture;
    private static String password;

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        GlobalVar.password = password;
    }

    public static String getStatus() {
        return status;
    }

    public static void setStatus(String status) {
        GlobalVar.status = status;
    }

    public static String getPosterName() {
        return posterName;
    }

    public static void setPosterName(String posterName) {
        GlobalVar.posterName = posterName;
    }

    public static String getImageUrl() {
        return imageUrl;
    }

    public static void setImageUrl(String imageUrl) {
        GlobalVar.imageUrl = imageUrl;
    }

    public static String getForumPicture() {
        return forumPicture;
    }

    public static void setForumPicture(String forumPicture) {
        GlobalVar.forumPicture = forumPicture;
    }
}
