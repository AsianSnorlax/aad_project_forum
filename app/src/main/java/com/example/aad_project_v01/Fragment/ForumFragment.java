package com.example.aad_project_v01.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.aad_project_v01.Adaptor.ForumListAdaptor;
import com.example.aad_project_v01.CreateForumActivity;
import com.example.aad_project_v01.Forum.Forum;
import com.example.aad_project_v01.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ForumFragment extends Fragment  {
    FloatingActionButton createForumButton;
    private RecyclerView recyclerView;
    DatabaseReference databaseReference;

    private ForumListAdaptor forumListAdaptor;
    private List<Forum> mForums;

    EditText searchForums;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forum, container, false);

        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mForums = new ArrayList<>();
        createForumButton = view.findViewById(R.id.create_forum);
        createForumButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), CreateForumActivity.class));
            }
        });
        readUsers();
        searchForums = view.findViewById(R.id.search_forum);
        searchForums.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                searchForums(s.toString().toLowerCase());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return view;
    }
    //Search for User with same name as entered text
    private void searchForums(String s){
        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        Query query = FirebaseDatabase.getInstance().getReference("Forum").orderByChild("searchTitle").startAt(s).endAt(s+"\uf8ff");

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mForums.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Forum forums = snapshot.getValue(Forum.class);
                    mForums.add(forums);

                }
                forumListAdaptor = new ForumListAdaptor(getContext(), mForums);
                recyclerView.setAdapter(forumListAdaptor);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    //Display Forum
    private void readUsers(){
        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Forum");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (searchForums.getText().toString().equals("")) {
                    mForums.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Forum forums = snapshot.getValue(Forum.class);
                        mForums.add(forums);
                    }
                    forumListAdaptor = new ForumListAdaptor(getContext(), mForums);
                    recyclerView.setAdapter(forumListAdaptor);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }
}
