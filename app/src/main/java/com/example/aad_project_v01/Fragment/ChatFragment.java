package com.example.aad_project_v01.Fragment;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.aad_project_v01.Adaptor.ForumListAdaptor;
import com.example.aad_project_v01.Adaptor.UserAdaptor;
import com.example.aad_project_v01.Forum.Forum;
import com.example.aad_project_v01.R;
import com.example.aad_project_v01.User.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ChatFragment extends Fragment {
    private RecyclerView recyclerView;
    private UserAdaptor userAdaptor;
    private List<User> mUsers;

    private RecyclerView recyclerView1;
    DatabaseReference forumReference;

    private ForumListAdaptor forumListAdaptor;
    private List<Forum> mForums;
    View view;
    FirebaseUser fbUser;
    DatabaseReference dbReference;
    private List <String> usersList;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(this.getResources().getConfiguration().orientation==Configuration.ORIENTATION_PORTRAIT) {
            view = inflater.inflate(R.layout.fragment_chat, container, false);
        }
        else{
            view = inflater.inflate(R.layout.fragment_chat2, container, false);
        }
        fbUser = FirebaseAuth.getInstance().getCurrentUser();

        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView1 = view.findViewById(R.id.recycler_view1);
        recyclerView1.setHasFixedSize(true);
        recyclerView1.setLayoutManager(new LinearLayoutManager(getContext()));
        mForums = new ArrayList<>();
        forumReference = FirebaseDatabase.getInstance().getReference("Forum");

        forumReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    mForums.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Forum forums = snapshot.getValue(Forum.class);
                        if (fbUser.getUid().equals(forums.getPosterId())) {
                            mForums.add(forums);
                        }
                    }
                    forumListAdaptor = new ForumListAdaptor(getContext(), mForums);
                    recyclerView1.setAdapter(forumListAdaptor);
                }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });


        usersList = new ArrayList<>();
        //Shows users that have interacted with User
        dbReference = FirebaseDatabase.getInstance().getReference("Chat Reference");
        dbReference.child(fbUser.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                usersList.clear();
                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                    String chatlist = snapshot.getValue(String.class);
                    usersList.add(chatlist);
                }
                chatList();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return view;
    }

    private void chatList(){
        mUsers = new ArrayList<>();
        dbReference = FirebaseDatabase.getInstance().getReference("Users");
        dbReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mUsers.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    User users = snapshot.getValue(User.class);
                    for (String chatlist : usersList){
                        if (users.getId().equals(chatlist)){
                            mUsers.add(users);
                        }
                    }
                }
                //Shows users that have interracted with User
                userAdaptor = new UserAdaptor(getContext(), mUsers, true);
                recyclerView.setAdapter(userAdaptor);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
