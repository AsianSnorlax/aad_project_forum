package com.example.aad_project_v01.Fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.aad_project_v01.User.User;
import com.example.aad_project_v01.R;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends Fragment {

    CircleImageView image_Profile;
    TextView nameText, statusText;

    DatabaseReference dbReference;
    FirebaseUser fbUser;
    Uri captureImageUri;
    String currentPhotoPath;

    StorageReference storageReference;
    private static final int IMAGE_REQUEST = 1;
    private Uri imageUri;
    private StorageTask uploadTask;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile,container,false);

        image_Profile = view.findViewById(R.id.profile_image);
        nameText = view.findViewById(R.id.name);
        statusText = view.findViewById(R.id.status);

        storageReference = FirebaseStorage.getInstance().getReference("uploads");
        fbUser = FirebaseAuth.getInstance().getCurrentUser();
        dbReference = FirebaseDatabase.getInstance().getReference("Users").child(fbUser.getUid());
        dbReference.addValueEventListener(new ValueEventListener() {
            @Override
            //Loads the images if the fragment is currently added to its activity.
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(isAdded()){
                    User user = dataSnapshot.getValue(User.class);
                    nameText.setText(user.getName());
                    statusText.setText(user.getstatus());
                    if(user.getImageURL().equals("default")){
                        image_Profile.setImageResource((R.mipmap.ic_launcher));
                    }else{
                        Glide.with(getContext()).load(user.getImageURL()).into(image_Profile);
                        //Glide.with(getActivity().getApplicationContext()).load(user.getImageURL()).into(image_Profile);
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        //Set what happens when you press the upload image button
        image_Profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder setProfilePic = new AlertDialog.Builder(getContext());
                setProfilePic.setMessage("Choose Image Location").setCancelable(true)
                        // (true) dialog can be ignored by pressing the back button
                        // Upload Profile Picture from Memory
                        .setPositiveButton("From Memory",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        openImage();
                                    }
                                })
                        .setNegativeButton("Capture Photo",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dispatchTakePictureIntent();
//                                        captureImage();
                                    }
                                }).show();
            }
        });
        return view;
    }

    //Take photo. After Taking photo, shows 3 options: Retake, Keep, Leave.
    static final int REQUEST_TAKE_PHOTO = 2;
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                captureImageUri = FileProvider.getUriForFile(getContext(),
                        "com.example.aad_project_v01",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, captureImageUri);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
    }

    //Create File for Photo
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

//    private void captureImage(){
//        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        takePictureIntent.putExtra("android.intent.extra.quickCapture",true);
//        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
//    }
    private void openImage(){
        Intent intentImage = new Intent();
        intentImage.setType("image/*");
        intentImage.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intentImage,IMAGE_REQUEST);
    }
    private String getFileExtension(Uri uri){
        ContentResolver contentResolver = getContext().getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }
    private void uploadImage(){
        final ProgressDialog proD = new ProgressDialog(getContext());
        proD.setMessage("Uploading....");
        proD.show();
        if (imageUri != null){
            final StorageReference fileReference = storageReference.child(System.currentTimeMillis()
                    +"."+getFileExtension(imageUri));
            uploadTask = fileReference.putFile(imageUri);
            uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()){
                        throw task.getException();
                    }
                    return fileReference.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()){
                        Uri downloadUri = task.getResult();
                        String mUri = downloadUri.toString();

                        dbReference = FirebaseDatabase.getInstance().getReference("Users").child(fbUser.getUid());
                        HashMap<String, Object>map = new HashMap<>();
                        map.put("imageURL", mUri);
                        dbReference.updateChildren(map);
                        proD.dismiss();
                    }else{
                        Toast.makeText(getContext(),"It Failed!", Toast.LENGTH_SHORT).show();
                        proD.dismiss();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    proD.dismiss();
                }
            });
        }else {
            Toast.makeText(getContext(), "No Image Selected", Toast.LENGTH_SHORT).show();
        }
    }
    static String selectedImagePath1;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        //From Memory
        if (requestCode == IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData()!=null) {
            imageUri = data.getData();
        }
        //From Camera
        if (requestCode == REQUEST_TAKE_PHOTO) {
            imageUri = captureImageUri;
        }
        if (uploadTask!=null && uploadTask.isInProgress()){
            Toast.makeText(getContext(), "Waiting.....", Toast.LENGTH_SHORT).show();
        }else{
            uploadImage();
        }
    }
}

