package com.example.aad_project_v01.Adaptor;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.aad_project_v01.Forum.ForumPost;
import com.example.aad_project_v01.R;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

public class CommentAdaptor extends RecyclerView.Adapter<CommentAdaptor.ViewHolder> {
    
    private Context mContext;
    private List<ForumPost> mPost;

    FirebaseUser fbUSER;

    public CommentAdaptor(Context mContext, List<ForumPost> mPost){
        this.mPost = mPost;
        this.mContext = mContext;
    }
    @NonNull
    @Override
    public CommentAdaptor.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.forum_comment_element, viewGroup, false);
        return new CommentAdaptor.ViewHolder(view);


    }
    @Override
    public void onBindViewHolder(@NonNull CommentAdaptor.ViewHolder viewHolder, int i) {

        ForumPost rPost = mPost.get(i);

        viewHolder.forum_Comment.setText(rPost.getForumMessage());
        viewHolder.poster_Name.setText(rPost.getName());
        viewHolder.status.setText(rPost.getStatus());

        if (rPost.getImageURL().equals("default")){
            viewHolder.profile_Image.setImageResource(R.mipmap.ic_launcher);
        }else{
            Glide.with(mContext).load(rPost.getImageURL()).into(viewHolder.profile_Image);
        }

    }

    @Override
    public int getItemCount() {
        return mPost.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder{

        public TextView forum_Comment;
        public ImageView profile_Image;
        public TextView status;
        public TextView poster_Name;

        public ViewHolder(View itemView) {
            super(itemView);

            forum_Comment = itemView.findViewById(R.id.forum_comment);
            status = itemView.findViewById(R.id.user_status);
            profile_Image = itemView.findViewById(R.id.profile_image1);
            poster_Name = itemView.findViewById(R.id.poster_name);
        }
    }
}

