package com.example.aad_project_v01.Adaptor;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.aad_project_v01.Extra.GlobalVar;
import com.example.aad_project_v01.Forum.Forum;
import com.example.aad_project_v01.ForumActivity;
import com.example.aad_project_v01.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class ForumListAdaptor extends RecyclerView.Adapter<ForumListAdaptor.ViewHolder>{
    private Context mContext;
    private List<Forum> mForumList;
    public Context context;

    public ForumListAdaptor(Context mContext, List<Forum> mForumList){
        this.mForumList = mForumList;
        this.mContext = mContext;
    }
    @NonNull
    @Override
    public ForumListAdaptor.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.forum_list_element, viewGroup, false);
        return new ForumListAdaptor.ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ForumListAdaptor.ViewHolder viewHolder, int i) {
        final Forum forumList = mForumList.get(i);
        viewHolder.forum_List_Title.setText(forumList.getTitle());
        viewHolder.forum_Comments.setText(Long.toString(forumList.getCommentsCount()));
        if (forumList.getFilePath().equals("default")){
            viewHolder.forum_Image.setImageResource(R.mipmap.ic_launcher);
        }else{
            Glide.with(mContext).load(forumList.getFilePath()).into(viewHolder.forum_Image);
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ForumActivity.class);
                intent.putExtra("threadId", forumList.getTitle()+'_'+forumList.getPosterId());
                mContext.startActivity(intent);
            }
        });
        try {
            if (GlobalVar.getStatus().equals("Admin")){
                viewHolder.itemView.setLongClickable(true);
                viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                        builder.setMessage("Confirm Delete?").setCancelable(true)
                                // (true) dialog can be ignored by pressing the back button
                                // Confirm Logout
                                .setPositiveButton("Delete",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                deleteForumThread(forumList);
                                            }
                                        })
                                // Misclick
                                .setNegativeButton("Cancel",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        }).show();

                        return true;
                    }
                });}
            else{
                viewHolder.itemView.setLongClickable(false);
            }

        }catch (Exception e) {

        }
    }
    public void deleteForumThread(Forum forumList){
        String key = forumList.getTitle()+'_'+forumList.getPosterId();
        //Delete Forum
        DatabaseReference deleteForum = FirebaseDatabase.getInstance().getReference().child("Forum");
        deleteForum.child(key).removeValue();

        DatabaseReference deleteForumList = FirebaseDatabase.getInstance().getReference().child("ForumPost");
        deleteForumList.child(key).removeValue();

    }
    @Override
    public int getItemCount() {
        return mForumList.size();
    }
    public  class ViewHolder extends RecyclerView.ViewHolder{
        public TextView forum_List_Title;
        public ImageView forum_Image;
        public TextView forum_Comments;

        public ViewHolder(View itemView) {
            super(itemView);
            forum_List_Title = itemView.findViewById(R.id.forum_list_title);
            forum_Image = itemView.findViewById(R.id.forum_image);
            forum_Comments = itemView.findViewById(R.id.forum_comment_count);

        }
    }

}
