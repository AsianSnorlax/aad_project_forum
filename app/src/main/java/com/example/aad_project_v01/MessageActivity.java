package com.example.aad_project_v01;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.aad_project_v01.Adaptor.MessageAdaptor;
import com.example.aad_project_v01.Chat.Chat;
import com.example.aad_project_v01.User.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
/**
 * This Activity is for Chatting other Users
 * @author Alex Tay
 * @since 14/10/19
 * @version Developer
 */
public class MessageActivity extends AppCompatActivity {

    CircleImageView profile_Image;
    TextView name;
    ImageButton btn_Send;
    EditText text_Send;
    MessageAdaptor messageAdapter;
    List<Chat> mChat;
    RecyclerView recyclerView;
    FirebaseUser fbUser;
    DatabaseReference dbReference;
    Intent intent;
    ValueEventListener seenListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                startActivity(new Intent(MessageActivity.this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        });

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);

        profile_Image = findViewById(R.id.profile_Image);
        name = findViewById(R.id.name);
        btn_Send = findViewById(R.id.btn_send);
        text_Send = findViewById(R.id.text_send);

        intent = getIntent();
        final String userid = intent.getStringExtra("userid");
        fbUser = FirebaseAuth.getInstance().getCurrentUser();

        //Set what happens when you press the send button(Green Arrow)
        btn_Send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = text_Send.getText().toString();        //Collect sent message as String
                if (!message.equals("")){
                    sendMessage(fbUser.getUid(), userid, message);      //Give String to method that uploads the message to the database
                }else{
                    Toast emptyMessage = Toast.makeText(MessageActivity.this, "There is nothing to send", Toast.LENGTH_LONG);
                    emptyMessage.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 1250);
                    emptyMessage.show();
                }
                text_Send.setText("");
            }
        });

        //Update Profile Picture and Name of Users Automatically
        dbReference = FirebaseDatabase.getInstance().getReference("Users").child(userid);
        //Load Image and Name of every newly registered account
        dbReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User users = dataSnapshot.getValue(User.class);
                name.setText(users.getName());
                if (users.getImageURL().equals("default")){
                    profile_Image.setImageResource(R.mipmap.ic_launcher);
                }else{
                    Glide.with(getApplicationContext()).load(users.getImageURL()).into(profile_Image);
                }
                readMessages(fbUser.getUid(), userid, users.getImageURL());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        seenMessage(userid);
    }
    private void seenMessage(final String userid){
        dbReference = FirebaseDatabase.getInstance().getReference("Chats");
        seenListener = dbReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Chat chats = snapshot.getValue(Chat.class);
                    if(chats.getReceiver().equals(fbUser.getUid())&&chats.getSender().equals(userid)){
                        HashMap<String, Object> hashMap = new HashMap<>();
                        hashMap.put("isseen", true);
                        snapshot.getRef().updateChildren(hashMap);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    //Send Message to Database along with the relevant information (Sender, Receiver, Actual Message, Seen status)
    private void sendMessage (final String sender, final String receiver, String message){
        DatabaseReference Chat_Reference_Sender = FirebaseDatabase.getInstance().getReference("Chat Reference").child(sender);
        DatabaseReference Chat_Reference_Receiver = FirebaseDatabase.getInstance().getReference("Chat Reference").child(receiver);
        Chat_Reference_Sender.child(receiver).setValue(receiver);
        Chat_Reference_Receiver.child(sender).setValue(sender);
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("sender", sender);
        hashMap.put("receiver", receiver);
        hashMap.put("message", message);
        hashMap.put("isseen", false);

        reference.child("Chats").push().setValue(hashMap);

    }
    private void readMessages(final String myid, final String userid, final String imageurl){
        mChat = new ArrayList<>();

        dbReference = FirebaseDatabase.getInstance().getReference("Chats");

        dbReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mChat.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Chat chat = snapshot.getValue(Chat.class);
                    if (chat.getReceiver().equals(myid)&& chat.getSender().equals(userid)||
                            chat.getReceiver().equals(userid)&&chat.getSender().equals(myid) ){
                        mChat.add(chat);
                    }
                    messageAdapter = new MessageAdaptor(MessageActivity.this, mChat, imageurl);
                    recyclerView.setAdapter(messageAdapter);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

}
