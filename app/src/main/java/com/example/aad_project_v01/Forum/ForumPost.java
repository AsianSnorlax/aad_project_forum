package com.example.aad_project_v01.Forum;

public class ForumPost {
    private String forumMessage;
    private String name;
    private String imageURL;
    private String status;

    public ForumPost(String forumMessage, String name, String imageURL, String status) {
        this.forumMessage = forumMessage;
        this.name = name;
        this.imageURL = imageURL;
        this.status =status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ForumPost() {}
    public String getForumMessage() {
        return forumMessage;
    }

    public void setForumMessage(String forumMessage) {
        this.forumMessage = forumMessage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
