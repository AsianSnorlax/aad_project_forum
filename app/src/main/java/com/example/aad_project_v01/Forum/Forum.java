package com.example.aad_project_v01.Forum;

public class Forum {
    private long commentsCount;
    private String contents;
    private String filePath;
    private String posterId;
    private String posterName;
    private String searchTitle;
    private String title;


    public Forum(long commentsCount, String forumContents, String filePath, String posterId, String posterName, String searchTitle, String title) {
        this.commentsCount = commentsCount;
        this.contents = forumContents;
        this.filePath = filePath;
        this.posterId = posterId;
        this.posterName = posterName;
        this.searchTitle = searchTitle;
        this.title = title;
    }

    public Forum() {
    }

    public String getPosterName() {
        return posterName;
    }

    public void setPosterName(String posterName) {
        this.posterName = posterName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getPosterId() {
        return posterId;
    }

    public void setPosterId(String posterId) {
        this.posterId = posterId;
    }

    public String getSearchTitle() {
        return searchTitle;
    }

    public void setSearchTitle(String searchTitle) {
        this.searchTitle = searchTitle;
    }

    public long getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(long commentsCount) {
        this.commentsCount = commentsCount;
    }
}
