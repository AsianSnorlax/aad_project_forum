package com.example.aad_project_v01.Forum;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ForumTest {
    private Forum forum = new Forum(1,"basic income?","www.123.com","2ada","Joel","andrewyang","AndrewYang");

    @Test
    public void getPosterName() {
        assertEquals("Joel",forum.getPosterName());
    }
    @Test
    public void getTitle() {
        assertEquals("AndrewYang",forum.getTitle());
    }
    @Test
    public void getContents() {
        assertEquals("basic income?",forum.getContents());
    }
    @Test
    public void getFilePath() {
        assertEquals("www.123.com",forum.getFilePath());
    }
    @Test
    public void getPosterId() {
        assertEquals("2ada",forum.getPosterId());
    }
    @Test
    public void getSearchTitle() {
        assertEquals("andrewyang",forum.getSearchTitle());
    }
    @Test
    public void getCommentsCount() {
        assertEquals(1,forum.getCommentsCount());
    }
}