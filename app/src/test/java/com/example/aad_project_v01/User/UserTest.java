package com.example.aad_project_v01.User;

import org.junit.Test;

import static org.junit.Assert.*;

public class UserTest {
    private User user = new User("abc123", "Marcel", "www.abc.com", "offline", "marcel", "Studying");
    @Test
    public void getstatus() {
        assertEquals("Studying",user.getstatus());
    }
    @Test
    public void onlinestatus() {
        assertEquals("offline",user.getOnlineStatus());
    }
    @Test
    public void getId() {
        assertEquals("abc123",user.getId());
    }
    @Test
    public void getName() {
        assertEquals("Marcel",user.getName());
    }
    @Test
    public void getImageURL() {
        assertEquals("www.abc.com",user.getImageURL());
    }
    @Test
    public void getSearch() {
        assertEquals("marcel",user.getSearch());
    }
}