package com.example.aad_project_v01.Chat;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ChatTest {
    private Chat chat = new Chat("judith","Kendrick","Hello!",false);

    @Test
    public void getSender() {
        assertEquals("judith",chat.getSender());
    }

    @Test
    public void getReceiver() {
        assertEquals("Kendrick",chat.getReceiver());
    }

    @Test
    public void getMessage() {
        assertEquals("Hello!",chat.getMessage());
    }
}